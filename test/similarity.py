# -*- coding: utf-8 -*-
"""
Created on Thu Aug  3 19:27:43 2017

@author: evgeny
"""

import pickle

from functions import tokenizer

from sklearn.feature_extraction.text import TfidfVectorizer

# load the data

X = pickle.load(open('data/tf_idf.pk', 'rb' ))

# get the scalar product (no need for normalization because tf-idf already do it)

pairwise_similarity_X = X * X.T
pairwise_similarity_X = pairwise_similarity_X.A 

# to check do the same for the short text example

text = ["человек ремонтирует квартиру", 
    "люди ремонтируют квартиры",
    "мама любит сына"]

# vectorize the text


tf_idf = TfidfVectorizer(tokenizer=tokenizer,analyzer=u'word')
Y = tf_idf.fit_transform(text)

# get the scalar product (no need for normalization because tf-idf already do it)

pairwise_similarity_Y = Y * Y.T
pairwise_similarity_Y = pairwise_similarity_Y.A