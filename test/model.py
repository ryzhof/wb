# -*- coding: utf-8 -*-
"""
Created on Sat Jul 29 19:54:17 2017

@author: evgeny
"""
import pickle
import pandas as pd
from numpy import hstack
import numpy as np

from sklearn.cluster import KMeans

import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

# download the data

X = pickle.load(open('data/tf_idf.pk', 'rb' ))
X = X[:5000]
data = pd.read_pickle('data/data.pkl')
data = data[:5000]

# setup the model

num_clusters = 3
num_seeds = 10
max_iterations = 300

model = KMeans(n_clusters=num_clusters,
            init = 'k-means++',
            n_init=10,
            max_iter=max_iterations,
            tol=0.0001,
            n_jobs=4)

labels = model.fit_predict(X)

# get the resulted dataframe

data['clasters'] = labels

clusters_distribution = data['clasters'].value_counts()

# PCA vizualization

pca_num_components = 2 # 2-d plot

labels_color_map = {
    0: '#20b2aa', 1: '#ff7373', 2: '#ffe4e1', 3: '#005073', 4: '#4d0404',
    5: '#ccc0ba', 6: '#4700f9', 7: '#f6f900', 8: '#00f91d', 9: '#da8c49'
}

# PCA vizualization

X = X.todense()

Y = PCA(n_components=pca_num_components).fit_transform(X)

fig, ax = plt.subplots()
for index, instance in enumerate(Y):
    # print instance, index, labels[index]
    Y_1, Y_2 = Y[index]
    color = labels_color_map[labels[index]]
    plt.scatter(Y_1, Y_2, c=color)

plt.title("Principal component analysis (PCA)")
plt.show()

# t-SNE vizualization
tsne_num_components = 2
embeddings = TSNE(n_components=tsne_num_components)
Y = embeddings.fit_transform(X)

for index, instance in enumerate(Y):
    # print instance, index, labels[index]
    Y_1, Y_2 = Y[index]
    color = labels_color_map[labels[index]]
    plt.scatter(Y_1, Y_2, c=color)

plt.title("t-SNE")
plt.show()