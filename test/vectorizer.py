# -*- coding: utf-8 -*-
"""
Created on Sat Jul 29 19:54:17 2017

@author: evgeny
"""
import pandas as pd

from functions import text_tuning
from functions import tokenizer

from nltk.corpus import stopwords

from sklearn.feature_extraction.text import TfidfVectorizer

import pickle

# download the data

data_source = pd.read_pickle('data/data.pkl')
data = pd.read_pickle('data/data.pkl')

# prepare the text for the vectorization (see the functions.py)
    
data['query'] = text_tuning(data['query'])

# set the stop-words list

stopwords = set(stopwords.words('russian'))

# Tfidf vectorization

tf_idf = TfidfVectorizer(tokenizer=tokenizer,analyzer=u'word',stop_words=stopwords,min_df=2)
X = tf_idf.fit_transform(data['query'])

# save the vectorized data

with open('data/tf_idf.pk', 'wb') as save_tf_idf:
    pickle.dump(X, save_tf_idf)