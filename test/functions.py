# -*- coding: utf-8 -*-
"""
Created on Mon Jul 31 11:30:59 2017

@author: evgeny
"""

import pymorphy2

from xml.dom import minidom
from urllib.request import urlopen

def yandex_data_1K():
    
    #this function collect 1K yandex queries and put it in list
      
    url = 'https://export.yandex.ru/last/last20x.xml' # define XML location
    dom = minidom.parse(urlopen(url)) # parse the data

    link = dom.getElementsByTagName('item') # set the target tag
    
    x = [] # set list
    for items in link:
        x.append(items.firstChild.nodeValue)
    
    return x

def text_tuning(dataframe_column):
    
    # this functions prepare the text for the vectorization
    
    dataframe_column = dataframe_column.str.lower() # lowering
    dataframe_column = dataframe_column.replace('[^а-я0-9]', ' ', regex=True) # non symbol -> space 
    dataframe_column = dataframe_column.replace(' +', ' ', regex=True) # many spaces -> one space
    dataframe_column = dataframe_column.replace('^ +', '', regex=True) # remove space at the begining of string
    dataframe_column = dataframe_column.replace('$ +', '', regex=True) # remove space at the end of string
    #dataframe_column = dataframe_column[dataframe_column != ''] # empty -> nan
    
    return dataframe_column

def tokenizer(s):

    # this function (custom tokenizer) provide the normalization
    # based on pymorphy2
    
    morph = pymorphy2.MorphAnalyzer()
    t = s.split(' ')
    f = []
    for j in t:
        m = morph.parse(j)
        if len(m) != 0:
            wrd = m[0]
            #if wrd.tag.POS not in ('NUMR','PREP','CONJ','PRCL','INTJ'):
            f.append(wrd.normal_form)
    return f