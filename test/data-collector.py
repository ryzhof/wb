# -*- coding: utf-8 -*-

import pandas as pd

from functions import yandex_data_1K

# collect 10K queries

data = []
for i in range(10):
    data = data + yandex_data_1K()

# save data to pickle

df = pd.DataFrame(data, columns=["query"])
df.to_pickle('data/data.pkl')
